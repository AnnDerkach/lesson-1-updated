
public class Calculator {
	
	public int calc(int a,int b,String opr){
		try{
			switch(opr){
				case "add":
					return a+b;
				case "sub":
					return a-b;
				case "mul":
					return a*b;
				case "div":
					if(b==0)
						throw new Exception ("Division by 0");	
					return a/b;	
				case "mod":
					return a%b;	
				default:
					System.err.println("There is no such operation");
					System.exit(1);
			}		
		}
		catch(Exception e){
			System.err.println(e); 
		}
		return 0;
	}
	
}
