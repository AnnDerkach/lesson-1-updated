
public class Base {
	
	public static void main(String[] args){
		try{
			IOdata io=new IOdata();
			Calculator cl=new Calculator();
			io.checkData(args);
			System.out.println(io.getA()+" "+io.getB());
			int result=cl.calc(io.getA(),io.getB(),io.getOperator());
			io.output(result);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
