import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;


public class IOdata {
	private String opr;
	private Integer a;
	private Integer b;
		
	public void checkData(String[]args) throws Exception{ 
		try {	
			if(args.length>3 || args.length<3)
				throw new Exception("Length of []args must be 3");
			a=Integer.parseInt(args[0]);
			b=Integer.parseInt(args[1]);
			opr=args[2];
			if(a>Integer.MAX_VALUE || b>Integer.MAX_VALUE)
				throw new Exception ("Numbers exceed the limits");
			if(a<Integer.MIN_VALUE || b<Integer.MIN_VALUE)
				throw new Exception ("Values are less than acceptable");	
		} 
		catch (Exception e) {
			System.err.println(e); 			
		}	
	}
	
	public void output(int result){ 
		try{
			System.out.println(result);
		}
		catch(Exception e){
			System.err.println("Change data type"); 
		}
		
	}
	
	public int getA(){
		return a;
	}
	
	public int getB(){
		return b;
	}
	
	public String getOperator(){
		return opr;
	}

}
